package com.lwoxLib.topic;

import java.io.Serializable;


public class Topic implements Serializable{
	
	
	private int feature;
	private String location;
	private String timestamp;
	private String actualValue;
	private String preferredValue;
	
	public Topic(int feature, String location){
		this.feature=feature;
		this.location=location;
		this.timestamp=null;
		this.actualValue=null;
		this.preferredValue=null;
	}
	
	public Topic(){
		
	}
	
	public int getFeature() {
		return feature;
	}

	public void setFeature(int feature) {
		this.feature = feature;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getActualValue() {
		return actualValue;
	}

	public void setActualValue(String value) {
		this.actualValue = value;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public String getPreferredValue() {
		return preferredValue;
	}

	public void setPreferredValue(String preferredValue) {
		this.preferredValue = preferredValue;
	}

}

package com.lwoxLib.enumeration;

public enum ROLE {

	//You can initialize enums using enumname(value)
	SENSOR_NEED(1),
	SENSOR_CAPABILITY(2),
	EXECUTOR_NEED(3),
	EXECUTOR_CAPABILITY(4);

	private int role;
	//Constructor which will initialize the enum
	ROLE(int r)
	{
		role = r;
	}

	//method to return the direction set by the user which initializing the enum
	public int getRole()
	{
		return role;
	}
}

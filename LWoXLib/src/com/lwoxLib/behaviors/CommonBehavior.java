package com.lwoxLib.behaviors;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.Messenger;
import android.util.Log;
import android.widget.Toast;
/**
 * This class implements the service communication methods in common between all the role behaviors
 * @author Gianluca De Mitri
 *
 */
public class CommonBehavior {

	Messenger mService = null;
	boolean mBound;
	IBinder helpBinder;
	Context c;
	
	/**
	 * Check if the service is started
	 * @return
	 */
	public boolean checkServiceStarted(){

		ActivityManager manager = (ActivityManager) c.getSystemService(Context.ACTIVITY_SERVICE);
		for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
			if ("com.lwox.core.LWoxService".equals(service.service.getClassName())) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * This method retrieves an IBinder and instantiates a Messenger object 
	 */
	private ServiceConnection mConnection = new ServiceConnection() {
		@Override
		public void onServiceConnected(ComponentName className, IBinder service) {
			// This is called when the connection with the service has been
			// established, giving us the object we can use to
			// interact with the service.  We are communicating with the
			// service using a Messenger, so here we get a client-side
			// representation of that from the raw IBinder object.
			helpBinder=service;
			Log.d("SERVICE","Service Connected");
			mService = new Messenger(service);
			mBound = true;
		}
		@Override
		public void onServiceDisconnected(ComponentName className) {
			// This is called when the connection with the service has been
			// unexpectedly disconnected -- that is, its process crashed.
			Log.d("SERVICE","Service disconnected");
			mService = null;
			mBound = false;
		}

	};

	/**
	 * This method allows to bind and open a communication channel with a service
	 * @return
	 */
	public Messenger doBindService() {
		String TAG="SERVICE";
		// Establish a connection with the service.  We use an explicit
		// class name because there is no reason to be able to let other
		// applications replace our component.
		//bindService(new Intent(this, MessengerService.class), mConnection, Context.BIND_AUTO_CREATE);
		//while(!checkServiceStarted() || mBound==false)
		//{
			try {
				Intent intentForMcuService = new Intent();
				//ComponentName component = new ComponentName("com.example.sensorservice", "SensorService");
				Log.d(TAG, "Before init intent.componentName");
				intentForMcuService.setAction("LWoX.service");
				intentForMcuService.setPackage("com.lwox.core");
				//Log.d(TAG, "Before bindService");
				if (c.bindService(intentForMcuService, mConnection, Context.BIND_AUTO_CREATE)){
					Log.d(TAG, "Bind.");
					mBound = true;
				} else {
					Log.d(TAG, "Binding to LWox returned false");
					Toast.makeText(c, "Service is not started!! Start it!!!", Toast.LENGTH_LONG).show();
				}
			} catch (SecurityException e) {
				Log.e(TAG, "can't bind to LWox, check permission in Manifest");
			}
		//}
		return mService;
	}

	/**
	 * This method allows to unbind a service
	 */
	public void unBindService(){
		c.unbindService(mConnection);
	}
}

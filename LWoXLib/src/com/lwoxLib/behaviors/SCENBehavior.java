package com.lwoxLib.behaviors;

import android.content.Context;
import android.os.Bundle;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;

import com.lwoxLib.topic.Topic;
/**
 * This class implements the service communication methods in common between Source capability and Executor need role
 * @author Gianluca De Mitri
 *
 */
public class SCENBehavior extends CommonBehavior {


	
	public SCENBehavior(Context context){
		this.c=context;
	}

	/**
	 * This method creates and sends a message to the service 
	 * @param mex
	 */
	public void send(Topic mex) {
		//if (!mBound) return;
		Log.d("APP", "Sending");
		// Create and send a message to the service, using a supported 'what' value
		Message msg = new Message();
		Bundle b = new Bundle();
		b.putSerializable("adaptor", mex);
		msg.setData(b);
		try {
			mService.send(msg);
			Log.d("APP", "Sent");
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}

}

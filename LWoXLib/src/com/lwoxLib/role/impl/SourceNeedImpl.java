package com.lwoxLib.role.impl;

import java.util.ArrayList;

import com.lwoxLib.behaviors.SNECBehavior;
import com.lwoxLib.role.SourceNeed;
import com.lwoxLib.topic.Topic;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;

public class SourceNeedImpl implements SourceNeed {

	SNECBehavior snec;
	private BroadcastReceiver receiver=null;
	private String intentToSubscribe=null;
	private Boolean asyncType=false;
	private Topic topic;
	private Context c;

	/**
	 * Use this constructor for normal sensor need behavior
	 * @param pContext
	 * @param pReceiver
	 */
	public SourceNeedImpl(Context pContext, BroadcastReceiver pReceiver) {
		//super(params.getContext());
		snec=SNECBehavior.getInstance(pContext);
		this.c=pContext;
		this.receiver=pReceiver;
		topic=new Topic();
		//behavior();
	}

	/**
	 * Use this constructor to send synchronous request 
	 * @param pContext
	 */
	public SourceNeedImpl(Context pContext) {
		//super(params.getContext());
		snec=SNECBehavior.getInstance(pContext);
		this.c=pContext;
		asyncType=true;
		topic=new Topic();
		//behavior();
	}
	
	public SourceNeedImpl() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Send synchronous request to retrieve topic actual value
	 */
	@Override
	public SourceNeedImpl sendSyncRequestForTopicActualValue(Handler handler) {

		topic.setActualValue(null);
		topic.setPreferredValue(null);
		topic.setTimestamp(null);

		//if(topic.getTimestamp()==null)
		snec.sendQueryTopic(topic, handler);

		return this;
	}

	/**
	 * gets actual value of topic retrieve from synchronous request
	 */
	@Override
	public String getSyncTopicActualValue(Message message){

		this.topic=snec.receiveTopicQueried(message);
		return topic.getActualValue();
	}

	/**
	 * gets actual value
	 */
	@Override
	public String getTopicActualValue(Intent intent) {

		this.topic=snec.receiveBroadcastData(intent);
		return topic.getActualValue();

	}

	/**
	 * subscribe or bind to the service in base of the operation to perform
	 */
	@Override
	public void behavior() {

		if(!asyncType)
		{							
			snec.subscribe(intentToSubscribe, receiver);
		}
		else
		{
			snec.doBindService();
		}
	}


	@Override
	public void destroy() {

		if(!asyncType)
			snec.unSubscribe(receiver);
		else
			snec.unBindService();

	}

	@Override
	public void setTopic(Topic topic){

		this.topic=topic;
		if(!asyncType)
			this.intentToSubscribe="topic."+topic.getFeature()+"."+topic.getLocation();
		behavior();
	}

	/**
	 * send adaptor list to the service
	 */
	@Override
	public void activeAdaptor(ArrayList<String> list) {
		
		snec.activeRequestAdaptor(list);
		
	}

}

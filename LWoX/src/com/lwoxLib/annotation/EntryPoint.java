package com.lwoxLib.annotation;
/**
 * This annotation is used to mark the adaptor entry point class
 */
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface EntryPoint {

	
}

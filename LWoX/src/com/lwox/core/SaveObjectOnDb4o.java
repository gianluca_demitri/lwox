package com.lwox.core;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import com.db4o.ObjectSet;
import com.lwox.dao.TopicDao;
import com.lwox.observer.HandleDataObserver;
import com.lwoxLib.topic.Topic;
/**
 * This AsyncTask manages the communications with the database
 * @author Gianluca De Mitri
 *
 */
//public class SaveObjectOnDb4o extends AsyncTask<Topic, Void, Void>
public class SaveObjectOnDb4o extends AsyncTask<Topic, Void, Void>
{
	Context c;
	TopicDao dao;
	HandleDataObserver h;
	Messenger m;

	public SaveObjectOnDb4o(Context context, Messenger message)
	{
		h = new HandleDataObserver(context);
		c = context;
		dao = new TopicDao(c);
		dao.addObserver(h);
		m=message;
	}

	protected Void doInBackground(Topic... topic)
	{
		Topic t;
		if(m!=null){		
			ObjectSet res=dao.queryByExample(topic[0]);
			t=(Topic) res.next();
			replyToClient(t);		
		}
		else
		{
			try
			{
				dao.update(topic[0]);

			}
			catch (Throwable e)
			{

				e.printStackTrace();
			}
		}
		dao.close();
		return null;
	}

	void replyToClient(Topic topic){
		Message msg=new Message();
		Bundle b=new Bundle();
		b.putSerializable("response", topic);
		msg.setData(b);
		msg.replyTo=m;
		try {
			msg.replyTo.send(msg);
		} catch (RemoteException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
}
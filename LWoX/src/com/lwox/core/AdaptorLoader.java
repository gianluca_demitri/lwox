package com.lwox.core;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Map;

import android.content.Context;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import com.lwox.parameters.Parameters;
import com.lwox.utility.StateManager;
import com.lwoxLib.annotation.EntryPoint;

import dalvik.system.DexClassLoader;
import dalvik.system.DexFile;

/**
 * This class handles the loading of the adaptors using DexFile, DexClassLoader and Java Reflection    
 * @author Gianluca De Mitri
 *
 */
public class AdaptorLoader {

	static Context context;
	static DexFile dexFile;
	static DexClassLoader loader;
	static String className=null;
	static Class clazz=null;
	static File tmpdir;
	static Object obj=null;
	/**
	 * This method loads the adaptor code, start the adaptor, check permission and update state.
	 * @param c
	 * @param adaptor
	 * @param map
	 * @param isRestarted
	 * @return map
	 */
	public static Map<String, Object> loadAdaptor(Context c, ArrayList<String> adaptor, Map<String, Object> map, Boolean isRestarted) {

		context=c;
		Log.d("SERVICE", "Adaptor file loading");		
		tmpdir = c.getDir("outdex", 0);
		tmpdir.mkdirs();
		Log.d("tmpdir", tmpdir.getAbsolutePath());

		if(!isRestarted){
			adaptor=StateManager.saveServiceState(context, adaptor);
			adaptor=StateManager.checkPermissions(c, adaptor);
		}

		if (!adaptor.isEmpty())
		{

			final Iterator<String> iterator=adaptor.iterator();

			while(iterator.hasNext()){	
				String name=iterator.next();
				obj=extractEntryClass(name);			
				map.put(name, obj); 
				startAdaptor(obj);
			}
			Log.d("SERVICE", "Adaptor file started");
			Toast.makeText(context, "Adaptor file started", Toast.LENGTH_LONG).show();
		}
		return map;
	}


	/**
	 * This method stops the selected adaptors
	 * @param c
	 * @param adaptor
	 * @param map
	 * @return map
	 */
	public static Map<String, Object> stopSelected(Context c, ArrayList<String> adaptor, Map<String, Object> map){
		context=c;

		if (!adaptor.isEmpty())
		{
			//for(File file : files){
			Iterator<String> iterator=adaptor.iterator();
			Object obj=null;
			while(iterator.hasNext()){	
				String name=iterator.next();
				for(Map.Entry<String, Object> entry : map.entrySet()){
					if(entry.getKey().equals(name)){
						obj=entry.getValue();
						stopAdaptor(obj);
						break;
					}
				}
				map.remove(name);
			}
		}
		return map;
	}


	/**
	 * This method extracts entry class from apk file using DexFile and DexClassLoader
	 * @param adaptorName
	 * @return Object
	 */
	protected static Object extractEntryClass(String adaptorName){

		Object obj=null;
		String path=Parameters.ADAPTOR_PATH+adaptorName;
		File file=new File(path);
		Log.d("FILE", "Loading "+file.getName());

		loader=new DexClassLoader(file.getAbsolutePath(), tmpdir.getAbsolutePath(), null, ClassLoader.getSystemClassLoader());				

		try {
			dexFile=DexFile.loadDex(file.getAbsolutePath(), tmpdir.getAbsolutePath()+"/"+file.getName(), 0);
			for(Enumeration<String> classNames = dexFile.entries(); classNames.hasMoreElements();) {
				className = classNames.nextElement();
				if(!className.contains("android")){							
					clazz=loader.loadClass(className);
					if(clazz.isAnnotationPresent(EntryPoint.class)){
						System.out.println("FIND");	
						//entryPointClasses.add(clazz);
						break;
					}
				}
			}
			//System.out.println("class: " + className);
			obj = clazz.newInstance();

		} catch (IOException | InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return obj;
	}

	/**
	 * This method stats adaptors calling start method of entry class
	 * @param obj
	 */
	protected static void startAdaptor(Object obj){
		if (obj != null)
		{
			//clazz.getMethod("start", Context.class).invoke(obj, c);
			Method[] list=clazz.getMethods();
			for(Method method: list){
				if(method.getName().equals("start")){
					try {
						method.invoke(obj, context);
					} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}
		else
		{
			Log.d("OBJ", "ERROR");
		}
	}

	/**
	 * This method stops adaptors selected calling stop method
	 * @param obj
	 */
	protected static void stopAdaptor(Object obj){
		if (obj != null)
		{
			//clazz.getMethod("start", Context.class).invoke(obj, c);
			Method[] list=obj.getClass().getMethods();
			for(Method method: list){
				if(method.getName().equals("stop")){
					try {
						method.invoke(obj);
					} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}
		else
		{
			Log.d("OBJ", "ERROR");
		}
	}
}

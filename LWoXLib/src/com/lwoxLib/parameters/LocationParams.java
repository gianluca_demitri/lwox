package com.lwoxLib.parameters;

public interface LocationParams {

	final String LOCATION_SELF="self";
    final String LOCATION_ANY="any";
}

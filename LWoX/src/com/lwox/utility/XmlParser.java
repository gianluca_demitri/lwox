package com.lwox.utility;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.util.Iterator;
import java.util.List;

import org.xmlpull.v1.XmlSerializer;

import com.lwox.parameters.Parameters;

import android.content.pm.FeatureInfo;
import android.hardware.Sensor;
import android.util.Log;
import android.util.Xml;
/**
 * This class implements a XML parser using to create a XML sensor list. 
 * @author Gianluca De Mitri
 *
 */
public class XmlParser implements Parameters {

	File newxmlfile;
	List<String> sensors;

	public XmlParser(List<String> list){
		sensors=list;
	}

	public void createXml(){

		Iterator<String> itr=sensors.iterator();

		newxmlfile = new File(XML_PATH + "Sensor.xml");
		try{
			if(newxmlfile.exists())
				newxmlfile.delete();
			else
				newxmlfile.createNewFile();
		}catch(IOException e)
		{
			Log.e("IOException", "Exception in create new File(");
		}
		FileOutputStream fileos = null;
		try{
			fileos = new FileOutputStream(newxmlfile);

		}catch(FileNotFoundException e)
		{
			Log.e("FileNotFoundException",e.toString());
		}
		XmlSerializer serializer = Xml.newSerializer();
		//StringWriter writer=new StringWriter();
		try{
			serializer.setOutput(fileos, "UTF-8");
			//serializer.setOutput(writer);
			serializer.startDocument("UTF-8", true);
			//serializer.setFeature("http://xmlpull.org/v1/doc/features.html#indent-output", true);
			//serializer.startTag(null, "root");
			serializer.startTag("", "sensors");
			while(itr.hasNext()){
				//for(int i=0; i<sensors.size(); i++){
				//serializer.startTag(null, "Sensor"+i);
				//serializer.attribute(null, "attribute", ((Sensor)sensors.get(i)).getName());
				//serializer.endTag(null, "Sensor"+i);
				serializer.startTag("", "sensor");
				serializer.text(itr.next());
				serializer.endTag("", "sensor");
			}
			serializer.endTag("", "sensors");

			serializer.endDocument();
			serializer.flush();
			fileos.close();
		}catch(Exception e)
		{
			Log.e("Exception","Exception occured in wroting");
		}
	}

}

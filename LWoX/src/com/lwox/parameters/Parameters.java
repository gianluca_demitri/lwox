package com.lwox.parameters;

import android.os.Environment;
import java.io.File;

public interface Parameters
{
  public static final String ADAPTOR_PATH = Environment.getExternalStorageDirectory().getAbsolutePath().toString() + "/LWoX/Adaptor/";
  public static final String INTENT_LWOX = "LWoX.service";
  public static final String XML_PATH = Environment.getExternalStorageDirectory().getAbsolutePath().toString() + "/LWoX/XML/";
  public static final String DB_PATH = Environment.getExternalStorageDirectory().getAbsolutePath().toString() + "/LWoX/DB/";
  //public static final String SERVICE_STATE_FILE = ADAPTOR_PATH+"/serviceState.txt";
  public static final String PREFERENCES="Adaptor";
  public static final String XML_SENSOR_FILE = XML_PATH+"Sensor.xml";
  
}

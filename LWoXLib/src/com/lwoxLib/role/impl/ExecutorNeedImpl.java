package com.lwoxLib.role.impl;

import java.sql.Timestamp;
import com.lwoxLib.behaviors.SCENBehavior;
import com.lwoxLib.role.ExecutorNeed;
import com.lwoxLib.topic.Topic;

import android.content.Context;

public class ExecutorNeedImpl implements ExecutorNeed {

	SCENBehavior scen;
	private Topic topic;
	private Context c;
	
	public ExecutorNeedImpl(Context context) {
		//super(params.getContext());
		this.c=context;
		scen=new SCENBehavior(c);
		topic=new Topic();
		behavior();
	}
	
	/**
	 * Sets preferred value of the topic sending to the service
	 */
	
	@Override
	public ExecutorNeedImpl setTopicPreferredValue(String value) {
		
		this.topic.setActualValue(null);
		this.topic.setPreferredValue(value);
		this.topic.setTimestamp(String.valueOf(new Timestamp(System.currentTimeMillis())));
		
		scen.send(this.topic);
		return this;
	}

	/**
	 * Bind the service
	 */
	@Override
	public void behavior() {
		
		scen.doBindService();
		
	}
	/**
	 * unBind the service
	 */
	@Override
	public void destroy() {
		scen.unBindService();		
	}

	@Override
	public void setTopic(Topic topic){
		
		this.topic=topic;

	}
}

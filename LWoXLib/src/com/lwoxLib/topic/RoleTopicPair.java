package com.lwoxLib.topic;

import java.lang.reflect.InvocationTargetException;

/**
 * This class creates a Topic-Role pair object
 * @author Gianluca
 *
 * @param <T>
 */
public class RoleTopicPair<T> {
	
	private T role;
	private Topic topic;
	
	
	
	public RoleTopicPair(T role, Topic topic){
		
		this.role=role;
		this.topic=topic;
		try {
			role.getClass().getMethod("setTopic", Topic.class).invoke(role, topic);
		} catch (IllegalAccessException | IllegalArgumentException
				| InvocationTargetException | NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}



	public T getRole() {
		return role;
	}



	public void setRole(T role) {
		this.role = role;
	}



	public Topic getTopic() {
		return topic;
	}



	public void setTopic(Topic topic) {
		this.topic = topic;
	}

}

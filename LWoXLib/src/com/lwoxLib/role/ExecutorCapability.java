package com.lwoxLib.role;

import java.util.ArrayList;

import android.content.Intent;

import com.lwoxLib.topic.Topic;
/**
 * 
 * This interface exposes all the methods related to Executor capability specific operation.
 *
 */
public interface ExecutorCapability {

	public String getTopicActualValue(Intent intent); 
	
	public String getTopicPreferredValue(Intent intent);

	public void behavior();
	
	public void setTopic(Topic topic);
	
	public void destroy();
	
	public void activeAdaptor(ArrayList<String> list);

}

package com.lwox.core;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.lwox.parameters.Parameters;
import com.lwox.builders.TopicBuilder;
import com.lwoxLib.topic.Topic;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Timestamp;
/**
 * This class instantiates an handler that manages all the messages sends by the binder applications.   
 * @author Gianluca De Mitri
 *
 */
public class IncomingHandler extends Handler implements Parameters
{
  Context c;
  Class clazz;
  Object obj;
  
  IncomingHandler(Context context)
  {
    c = context;
  }
  
  /**
   * This methods builds topic object from the topic message received using Java Reflection in order to bypass the use of different class loaders
   * by sender and receiver 
   * @param obj
   * @return
   * @throws IllegalAccessException
   * @throws IllegalArgumentException
   * @throws InvocationTargetException
   * @throws NoSuchMethodException
   */
 private Topic buildTopicObj(Object obj)
    throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException
  {
    TopicBuilder builder = new TopicBuilder();
    int feature = (Integer)obj.getClass().getMethod("getFeature").invoke(obj);
    String loc = (String)obj.getClass().getMethod("getLocation").invoke(obj);
    String timestamp = (String) obj.getClass().getMethod("getTimestamp").invoke(obj);
    String actualValue = (String)obj.getClass().getMethod("getActualValue").invoke(obj);
    String preferredValue=(String)obj.getClass().getMethod("getPreferredValue").invoke(obj);
    builder.setFeature(feature).setLocation(loc).setTimestamp(timestamp).setActualValue(actualValue).setPreferredValue(preferredValue);
    return builder.getTopic();
  }
 
  @Override
  public void handleMessage(Message msg)
  {
	Log.d("SERVICE", "Topic receiving...");
    Bundle bundle = msg.getData();
    try
    {
      Object obj = bundle.getSerializable("adaptor");
      Topic topic = buildTopicObj(obj);
      
      new SaveObjectOnDb4o(c, msg.replyTo).execute(topic, null, null);
      super.handleMessage(msg);
      return;
    }
    catch (IllegalArgumentException|IllegalAccessException|InvocationTargetException|NoSuchMethodException e)
    {
      e.printStackTrace();
    }
  }
  
  
}


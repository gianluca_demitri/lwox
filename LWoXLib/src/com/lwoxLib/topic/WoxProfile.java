package com.lwoxLib.topic;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Iterator;
import android.util.Log;
import com.lwoxLib.role.impl.ExecutorCapabilityImpl;
import com.lwoxLib.role.impl.ExecutorNeedImpl;
import com.lwoxLib.role.impl.SourceCapabilityImpl;
import com.lwoxLib.role.impl.SourceNeedImpl;
/**
 * This class stores in a ArrayList all the Role-Topic pairs created
 * @author Gianluca
 *
 */
public class WoxProfile {

	private static WoxProfile instance=null;

	private ArrayList<RoleTopicPair> pairs;

	/**
	 * create an instance of WoxProfile. It must be unique
	 * @return
	 */
	public static WoxProfile getInstance(){
		if(instance==null){
			instance=new WoxProfile();
			instance.pairs=new ArrayList<RoleTopicPair>();
		}
		return instance;
	}

	/** 
	 * adds a role-topic pair to the arrayList 
	 * @param pair
	 */
	public void add(RoleTopicPair pair){

		pairs.add(pair);
	}

	
	/**
	 * Delete a role-topic pair from arrayList according to the role and topic specified in the parameters
	 * @param role
	 * @param topic
	 */
	public void delete(int role, Topic topic){

		RoleTopicPair pair=retrievePair(role, topic);
		//pair.getRole().destroy();
		try {
			pair.getRole().getClass().getMethod("destroy").invoke(pair.getRole());
		} catch (IllegalAccessException | IllegalArgumentException
				| InvocationTargetException | NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		pairs.remove(retrievePair(role, topic));
	}
	
	/**
	 * delete all topic-role pair stored in array list 
	 */
	public void deleteAll(){
	
		RoleTopicPair currentPair;
		Iterator<RoleTopicPair> listIterator =pairs.iterator();
		while(listIterator.hasNext())
		{
			currentPair=listIterator.next();
			//currentPair.getRole().destroy();
			try {
				currentPair.getRole().getClass().getMethod("destroy").invoke(currentPair.getRole());
			} catch (IllegalAccessException | IllegalArgumentException
					| InvocationTargetException | NoSuchMethodException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		pairs.clear();
		
	}

	/*public Role retriveRole(int role, Topic topic){

		return retrievePair(role, topic).getRole();
	}*/

	/** 
	 * retrieve role-topic pair according to the role and topic specified in the parameters
	 * @param role
	 * @param topic
	 * @return
	 */
	public RoleTopicPair retrievePair(int role, Topic topic){

		RoleTopicPair currentPair;
		RoleTopicPair retrievedPair=null;
		Iterator<RoleTopicPair> listIterator =pairs.iterator(); 

		if(pairs.size()==1){
			Log.d("SIZE", "1");
			return pairs.get(0);
		}
		else
		{
			Log.d("SIZE", String.valueOf(pairs.size()));
			while(listIterator.hasNext())
			{
				currentPair=listIterator.next();
				/*if(currentPair!=null)
					Log.d("PAIR", "pair is not null");
				else
					Log.d("PAIR", "pair is null");*/
				switch(role){
				case 1:
					if(currentPair.getRole() instanceof SourceNeedImpl && currentPair.getTopic().getFeature()==topic.getFeature() && currentPair.getTopic().getLocation().equals(topic.getLocation())){
						retrievedPair=currentPair;
						Log.d("FIND", "FIND");
					}

					break;
				case 2:
					if(currentPair.getRole() instanceof SourceCapabilityImpl && currentPair.getTopic().getFeature()==topic.getFeature() && currentPair.getTopic().getLocation().equals(topic.getLocation())){
						retrievedPair=currentPair;
						Log.d("FIND", "FIND");
					}
					break;
				case 3:
					if(currentPair.getRole() instanceof ExecutorNeedImpl && currentPair.getTopic().getFeature()==topic.getFeature() && currentPair.getTopic().getLocation().equals(topic.getLocation())){
						retrievedPair=currentPair;
						Log.d("FIND", "FIND");
					}
					break;

				case 4: 
					if(currentPair.getRole() instanceof ExecutorCapabilityImpl && currentPair.getTopic().getFeature()==topic.getFeature() && currentPair.getTopic().getLocation().equals(topic.getLocation())){
						retrievedPair=currentPair;
						Log.d("FIND", "FIND");
					}
					break;
				}								
			}

			/*if(retrievedPair!=null)
				Log.d("PAIR", "retrieved pair is not null");
			else
				Log.d("PAIR", "retrieved pair is null");*/
			return retrievedPair;
		}

	}

}

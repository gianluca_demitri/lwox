package com.lwoxLib.role.impl;

import java.util.ArrayList;

import com.lwoxLib.behaviors.SNECBehavior;
import com.lwoxLib.role.ExecutorCapability;
import com.lwoxLib.topic.Topic;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class ExecutorCapabilityImpl implements ExecutorCapability{

	SNECBehavior snec;
	private BroadcastReceiver receiver;
	private String intentToSubscribe;
	private Topic topic;
	private Context c;
	

	public ExecutorCapabilityImpl(Context pContext, BroadcastReceiver pReceiver) {
		//super(params.getContext());
		this.c=pContext;
		this.receiver=pReceiver;
		topic=new Topic();
		//behavior();
	}
	/**
	 * @return actual value of updated topic
	 */
	
	@Override
	public String getTopicActualValue(Intent intent) {	
			
			this.topic=snec.receiveBroadcastData(intent);		
			return this.topic.getActualValue();	
	}
	
	/**
	 * @return preferred value of updated topic
	 */
	@Override
	public String getTopicPreferredValue(Intent intent) {
		
		this.topic=snec.receiveBroadcastData(intent);
		return this.topic.getPreferredValue();
		
	}

	/**
	 * subscribe the caller application to the service
	 */
	@Override
	public void behavior() {

		snec=SNECBehavior.getInstance(c);
		if(intentToSubscribe!=null && receiver!=null)
		{							
			snec.subscribe(intentToSubscribe, receiver);
		}
		else
			Log.e("ERROR","Intent and receiver are null");	

	}
	
	@Override
	public void setTopic(Topic topic){
		
		this.topic=topic;
		this.intentToSubscribe="topic."+topic.getFeature()+"."+topic.getLocation();
		behavior();
	}

	/**
	 * unsubscribe the caller application from the service
	 */
	@Override
	public void destroy() {
		snec.unSubscribe(receiver);

	}
	
	/**
	 * send adaptor list to the service
	 */
	@Override
	public void activeAdaptor(ArrayList<String> list) {
		
		snec.activeRequestAdaptor(list);
		
	}


}

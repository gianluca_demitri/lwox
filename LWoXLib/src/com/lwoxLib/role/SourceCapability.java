package com.lwoxLib.role;

import com.lwoxLib.role.impl.SourceCapabilityImpl;
import com.lwoxLib.topic.Topic;
/**
 * 
 * This interface exposes all the methods related to Source capability specific operation.
 *
 */
public interface SourceCapability {

	public SourceCapabilityImpl setTopicActualValue(String value); 

	public void behavior();
	
	public void setTopic(Topic topic);
	
	public void destroy();
}


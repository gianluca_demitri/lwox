package com.lwox.utility;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.lwox.parameters.Parameters;
/**
 * This class handles service state storing the started adaptors in the SheredPreferences component
 * @author Gianluca De Mitri
 *
 */
public class StateManager {

	private final static String TEXT_DATA_KEY = "adaptor";

	public static boolean isSavedInState(Context context, String adaptor){
		SharedPreferences preferences = context.getSharedPreferences(Parameters.PREFERENCES, Context.MODE_PRIVATE);
		Map<String, ?> map=preferences.getAll();
		for(Map.Entry<String, ?> entry : map.entrySet()){
			if(entry.getValue().equals(adaptor))
				return true;
		}
		return false;
	}

	public static ArrayList<String> loadServiceState(Context context){
		ArrayList<String> result=new ArrayList<String>();
		SharedPreferences preferences = context.getSharedPreferences(Parameters.PREFERENCES, Context.MODE_PRIVATE);
		Map<String, ?> map=preferences.getAll();
		for(Map.Entry<String, ?> entry : map.entrySet())
			result.add((String) entry.getValue());
		return result;
	}

	public static ArrayList<String> saveServiceState(Context context, ArrayList<String> adaptor){

		ArrayList<String> result=new ArrayList<String>();
		SharedPreferences prefs = context.getSharedPreferences(Parameters.PREFERENCES, Context.MODE_APPEND);
		SharedPreferences.Editor editor = prefs.edit();
		Map<String, ?> map=prefs.getAll();
		int i=map.size();
		Iterator<String> iterator=adaptor.iterator();
		while(iterator.hasNext()){
			String current=(String) iterator.next();
			if(!map.containsValue(current))
			{
				editor.putString(TEXT_DATA_KEY+i, current);
				editor.commit();
				result.add(current);
				i++;
			}
		}
		return result;
	}

	public static ArrayList<String> checkPermissions(Context context, ArrayList<String> adaptor){

		ArrayList<String> perm=null;
		ArrayList<String> result=new ArrayList<String>();
		Iterator iterator=adaptor.iterator();
		while(iterator.hasNext()) {
			String currentAdaptor=(String) iterator.next();
			String path=Parameters.ADAPTOR_PATH+currentAdaptor;
			perm=Utility.getAdaptorPermission(path);
			if(!Utility.checkPermission(perm, context)){
				Log.e("ERROR", "LWOX has't the permission to load "+currentAdaptor+" adaptor");
			}
			else
				result.add(currentAdaptor);
		}
		return result;
	}

	public static void stopSelected(Context context, ArrayList<String> selected){

		SharedPreferences prefs = context.getSharedPreferences(Parameters.PREFERENCES, Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = prefs.edit();
		Iterator iterator=selected.iterator();
		Map<String, ?> map=prefs.getAll();
		while(iterator.hasNext()){
			String current=(String) iterator.next();
			for (Map.Entry<String, ?> entry : map.entrySet()){
				if(entry.getValue().equals(current)){
					editor.remove(entry.getKey());
					editor.commit();
				}
				
			}
		}

	}

	/*public static void writeFile(String path, String data){
		try
		{
			//String filename= "MyFile.txt";
			FileWriter fw = new FileWriter(path, true); //the true will append the new data
			fw.append("\n"+data);
			fw.close();
		}
		catch(IOException ioe)
		{
			System.err.println("IOException: " + ioe.getMessage());
		}
	}

	public static ArrayList<String> readFile(String path){
		ArrayList<String> result=new ArrayList<String>();

		FileInputStream fstream;
		try {
			fstream = new FileInputStream(path);
			BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
			for(String line; (line = br.readLine()) != null; ) {
				//Log.d("LINE", line);
				if(line.length()!=0)
					result.add(line);
			}
			fstream.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}*/

	/*public static boolean isSavedInState(String path, String adaptor){

		FileInputStream fstream;
		try {
			fstream = new FileInputStream(path);
			BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
			for(String line; (line = br.readLine()) != null; ) {
				//Log.d("LINE", line);
				if(line.equals(adaptor))
					return true;

			}
			fstream.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}*/

}

package com.lwox.dao;



import com.db4o.ObjectSet;
import com.db4o.ext.DatabaseClosedException;
import com.db4o.ext.DatabaseReadOnlyException;
import com.lwoxLib.topic.Topic;

import android.content.Context;
import android.util.Log;

/**
 * This class extends Db4OGenericDao and implements methods needed to store and update the topic instance into the database.
 * Furthermore it notifies the observers of topic instance changes 
 * @author Gianluca De Mitri
 *
 */
public class TopicDao extends Db4OGenericDao<Topic>{

	/**
	 * @param ctx
	 */
	public TopicDao(Context ctx) {
		super(ctx);
	}

	public void update(Topic o) throws DatabaseClosedException, DatabaseReadOnlyException {

		Topic queryTopic=new Topic(o.getFeature(), o.getLocation());
		
		ObjectSet res=queryByExample(queryTopic);

		if(res.size()==0){
			Log.d("SERVICE", "Storing topic...");
			store(o);
			Log.d("SERVICE", "Topic Stored");
			notifyObserver(o);

		}
		else
		{
			Log.d("SERVICE", "Updating topic...");
			Topic t=null;
			String oldActualValue=null;
			String oldPreferredValue=null;
			
			while (res.hasNext()){
				t= (Topic) res.next();

				oldActualValue=t.getActualValue();
				oldPreferredValue=t.getPreferredValue();
				
				t=buildTopic(o, t);
				
				if(checkValueChanged(oldActualValue, o.getActualValue()) || checkValueChanged(oldPreferredValue, o.getPreferredValue())){
					store(t);				
					Log.d("SERVICE", "Topic updated");						
					notifyObserver(t);
				}
				
				ObjectSet res2=query(Topic.class);
				Log.d("SIZE DB", String.valueOf(res2.size()));
				
			}			


		}

	}
	Topic buildTopic(Topic incomingTopic, Topic retrieveTopic){
		
		if(incomingTopic.getActualValue()!=null)
			retrieveTopic.setActualValue(incomingTopic.getActualValue());
		if(incomingTopic.getPreferredValue()!=null)
			retrieveTopic.setPreferredValue(incomingTopic.getPreferredValue());
		retrieveTopic.setTimestamp(incomingTopic.getTimestamp());
		
		return retrieveTopic;
		
	}

	boolean checkValueChanged(String oldValue, String newValue){

		if(newValue!=null){
			if(!oldValue.equals(newValue))
				return true;
		}

		return false;
	}

	void notifyObserver(Topic t){
		Log.d("SERVICE", "Notifying to observer...");
		this.setChanged();
		this.notifyObservers(t);
	}
	
	

}
package com.lwox.core;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.lwox.parameters.Parameters;
import com.lwox.utility.StateManager;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.os.Messenger;
import android.util.Log;
import android.widget.Toast;
/**
 * This class manages the LWoX-service life cycle
 * @author Gianluca De Mitri
 *
 */
public class LWoxService extends Service implements Parameters {

	final Messenger mMessenger=new Messenger(new IncomingHandler(this));
	StartReceiver receiver = new StartReceiver();
	StopReceiver receiver2 = new StopReceiver();
	Map<String, Object> map;
	Context context=this;

	@Override
	public IBinder onBind(Intent intent) {
		Log.d("SERVICE", "entering onBind");
		return mMessenger.getBinder();	
	}
	//Comment
	@Override
	public void onCreate()
	{
		super.onCreate();
		map=new HashMap<String, Object>();

	}

	@Override
	public void onDestroy()
	{
		super.onDestroy();
		//adaptorLoader.cancel(true);
		//try {
		unregisterReceiver(receiver);
		unregisterReceiver(receiver2);
		//} catch(IllegalArgumentException e) {
		SharedPreferences preferences = context.getSharedPreferences(Parameters.PREFERENCES, Context.MODE_PRIVATE);
		preferences.edit().clear().commit();
		//}

		Log.d("SERVICE", "LWoX Stopped");
		Toast.makeText(this, "LWoX Stopped", Toast.LENGTH_LONG).show();
		/*if(entryPointClasses!=null)
			stopAdaptor();*/

	}


	@Override
	public int onStartCommand(Intent intent, int flags, int startId)
	{
		IntentFilter filter = new IntentFilter();
		filter.addAction("com.android.startAdaptor");
		registerReceiver(receiver, filter);

		IntentFilter filter2 = new IntentFilter();
		filter2.addAction("com.android.stopAdaptor");
		registerReceiver(receiver2, filter2);

		ArrayList<String> adaptor=StateManager.loadServiceState(this);
		if(adaptor!=null)
			if(!adaptor.isEmpty())
			{
				Intent it = new Intent("com.android.startAdaptor");
				it.putStringArrayListExtra("adaptor", adaptor);
				it.putExtra("restarted", true);
				sendBroadcast(it);
			}

		Log.d("SERVICE", "LWOX RUNNING");
		Toast.makeText(this, "LWoX Started", Toast.LENGTH_LONG).show();

		return START_STICKY;
	}

	/**
	 * This class will receive an intent broadcast sent by applications that need to start adaptors.  
	 * @author Gianluca De Mitri
	 *
	 */
	public class StartReceiver extends BroadcastReceiver {

		ArrayList<String> adaptor=null;

		@Override
		public void onReceive(Context context, Intent intent) {
			if ("com.android.startAdaptor".equalsIgnoreCase(intent.getAction())) {
				Log.d("ADAPTOR", "ADAPTOR");
				adaptor=intent.getStringArrayListExtra("adaptor");
				Boolean restarted=intent.getBooleanExtra("restarted", false);
				//if(restarted)
					map=AdaptorLoader.loadAdaptor(context, adaptor, map, restarted);
				//else
					//map=AdaptorLoader.loadAdaptor(context, adaptor, map, false);	
			} 
		}
	}

	public class StopReceiver extends BroadcastReceiver {

		ArrayList<String> adaptor=null;

		@Override
		public void onReceive(Context context, Intent intent) {
			if ("com.android.stopAdaptor".equalsIgnoreCase(intent.getAction())) {
				Log.d("ADAPTOR", "ADAPTOR");
				adaptor=intent.getStringArrayListExtra("adaptor");
				map=AdaptorLoader.stopSelected(context, adaptor, map);

			} 
		}
	}
}	
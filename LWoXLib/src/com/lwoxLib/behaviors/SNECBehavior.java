package com.lwoxLib.behaviors;

import java.util.ArrayList;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;

import com.lwoxLib.topic.Topic;

/**
 * This class implements the service communication methods in common between Source need and Executor capability role
 * @author Gianluca De Mitri
 *
 */

public class SNECBehavior extends CommonBehavior {
	

	public static SNECBehavior getInstance(Context context){
		SNECBehavior snec=new SNECBehavior();
		snec.c=context;
		return snec;		
	}

	/**
	 * This method allows to receive broadcast data on a specified intent
	 * @param intent
	 * @return
	 */
	public Topic receiveBroadcastData(Intent intent)
	{
		if (null == intent) return null;
		
		Topic mex =(Topic) intent.getBundleExtra("data").getSerializable("bundleobj");
		
		return mex;
	}
	
	/**
	 * This method subscribes an application on a specified intent
	 * @param intentToSubscribe
	 * @param receiver
	 */
	public void subscribe(String intentToSubscribe, BroadcastReceiver receiver){
		IntentFilter filter =new IntentFilter(intentToSubscribe);
		c.registerReceiver(receiver, filter);
	}
	
	/**
	 * This method unsubscribes an application on a specified intent
	 * @param intentToSubscribe
	 * @param receiver
	 */
	public void unSubscribe(BroadcastReceiver receiver){
		c.unregisterReceiver(receiver);
	}
	
	/**
	 * This method sends a synchronous request to retrieve the queried topic 
	 * @param mex
	 * @param handle
	 */
	public void sendQueryTopic(Topic mex, Handler handle) {
		if (!mBound) return;
		Log.d("APP", "Sending");

		// Create and send a message to the service, using a supported 'what' value
		Message msg = new Message();
		Bundle b = new Bundle();
		b.putSerializable("adaptor", mex);
		msg.setData(b);
		msg.replyTo=new Messenger(handle);
	
		try {
			mService.send(msg);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * This method retrieves the queried topic 
	 * @param mex
	 * @param handle
	 */
	public Topic receiveTopicQueried(Message msg){
		return (Topic) msg.getData().getSerializable("response");
	}
	
	/**
	 * This methods sends the adaptor list to the service needed to execute particular tasks
	 * @param list
	 */
	public void activeRequestAdaptor(ArrayList<String> list){
		Intent it = new Intent("com.android.startAdaptor");
		it.putStringArrayListExtra("adaptor", list);
		c.sendBroadcast(it);
		
	}

}

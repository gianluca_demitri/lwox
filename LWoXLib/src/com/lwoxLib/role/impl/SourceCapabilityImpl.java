package com.lwoxLib.role.impl;

import java.sql.Timestamp;

import com.lwoxLib.behaviors.SCENBehavior;
import com.lwoxLib.role.SourceCapability;
import com.lwoxLib.topic.Topic;
import android.content.Context;

public class SourceCapabilityImpl implements SourceCapability {

	SCENBehavior scen;
	private Topic topic;
	private Context c;
	
	public SourceCapabilityImpl(Context context) {
		//super(params.getContext());
		this.c=context;
		topic=new Topic();
		behavior();
	}
	
	/**
	 * Sets actual value of the topic sending to the service
	 */
	@Override
		public SourceCapabilityImpl setTopicActualValue(String value) {
			
			this.topic.setActualValue(value);
			this.topic.setPreferredValue(null);
			this.topic.setTimestamp(String.valueOf(new Timestamp(System.currentTimeMillis())));
			
			scen.send(this.topic);
			return this;
		}

	/**
	 * binds to the service
	 */
	@Override
	public void behavior() {
		
		scen=new SCENBehavior(c);
		scen.doBindService();
		
	}
	
	/**
	 * unbinds to the service
	 */
	@Override
	public void destroy() {
		scen.unBindService();
		
	}
	
	@Override
	public void setTopic(Topic topic){	
		
		this.topic=topic;

	}

}

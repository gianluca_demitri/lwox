package com.lwoxLib.role;

import com.lwoxLib.topic.Topic;
/**
 * 
 * This interface exposes all the methods related to Executor need specific operation.
 *
 */
public interface ExecutorNeed {

	public ExecutorNeed setTopicPreferredValue(String value); 

	public void behavior();
	
	public void setTopic(Topic topic);
	
	public void destroy();
}

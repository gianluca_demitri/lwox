package com.lwoxLib.role;

import java.util.ArrayList;

import android.content.Intent;
import android.os.Handler;
import android.os.Message;

import com.lwoxLib.role.impl.SourceNeedImpl;
import com.lwoxLib.topic.Topic;
/**
 * 
 * This interface exposes all the methods related to Source need specific operation.
 *
 */
public interface SourceNeed {
	
	public String getTopicActualValue(Intent intent); 
	
	public SourceNeedImpl sendSyncRequestForTopicActualValue(Handler handler);
	
	public String getSyncTopicActualValue(Message message);
	
	public void behavior();
	
	public void setTopic(Topic topic);
	
	public void destroy();
	
	public void activeAdaptor(ArrayList<String> list);
	

}

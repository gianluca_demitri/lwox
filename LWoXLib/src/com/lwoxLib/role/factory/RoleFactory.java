package com.lwoxLib.role.factory;

import android.content.BroadcastReceiver;
import android.content.Context;

import com.lwoxLib.role.ExecutorCapability;
import com.lwoxLib.role.ExecutorNeed;
import com.lwoxLib.role.SourceCapability;
import com.lwoxLib.role.SourceNeed;
import com.lwoxLib.role.impl.ExecutorCapabilityImpl;
import com.lwoxLib.role.impl.ExecutorNeedImpl;
import com.lwoxLib.role.impl.SourceCapabilityImpl;
import com.lwoxLib.role.impl.SourceNeedImpl;
/**
 * 
 * This factory class manages the creation of Role classes objects of any kind. 
 *
 */
public class RoleFactory {

	public static RoleFactory getInstance(){
		return new RoleFactory();
	}

	/*public static Role getRole(int role, RoleParams params){

		switch (role){

		case 1:
			//Role role1=(Role) new SensorNeedImpl(params);
			return new SensorNeedImpl(params);

		case 2:
			return new SensorCapabilityImpl(params);

		case 3: 
			return new ExecutorNeedImpl(params);

		case 4:
			return new ExecutorCapabilityImpl(params);

		default: 
			return null;
		}

	}*/
	/**
	 * Gets an instance of the <code>SourceNeed</code>.
	 * @param pContext
	 * @param pReceiver
	 * @return an instance of <code>SourceNeed</code>
	 */
	public static SourceNeed getSensorNeedRole(Context pContext, BroadcastReceiver pReceiver){

		return new SourceNeedImpl(pContext, pReceiver);
	}
	/**
	 * Gets an instance of the <code>SourceCapability</code>.
	 * @param context
	 * @return an instance of <code>SourceCapability</code>
	 */
	public static SourceCapability getSensorCapabilityRole(Context context){

		return new SourceCapabilityImpl(context);
	}

	/**
	 * Gets an instance of the <code>ExecutorNeed</code>.
	 * @param context
	 * @return an instance of <code>ExecutorNeed</code>
	 */
	public static ExecutorNeed getExecutorNeedRole(Context context){

		return new ExecutorNeedImpl(context);
	}

	/**
	 * Gets an instance of the <code>ExecutorCapability</code>.
	 * @param pContext
	 * @param pReceiver
	 * @return an instance of <code>ExecutorCapability</code>
	 */
	
	public static ExecutorCapability getExecutorCapabilityRole(Context pContext, BroadcastReceiver pReceiver){

		return new ExecutorCapabilityImpl(pContext, pReceiver);
	}
	
	/**
	 * Gets an instance of the <code>SensorNeed</code> calling the SensorNeed constructor with Context parameter.  
	 * @param pContext
	 * @return an instance of <code>SensorNeed</code>
	 */
	public static SourceNeed getSensorNeedRoleForSyncRequest(Context pContext){

		return new SourceNeedImpl(pContext);
	}

}





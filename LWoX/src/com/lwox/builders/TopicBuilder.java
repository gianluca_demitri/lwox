package com.lwox.builders;

import java.sql.Timestamp;

import com.lwoxLib.topic.Topic;

public class TopicBuilder {
	
	public static Topic topic;
	
	public TopicBuilder()
	{
		topic=new Topic();	
	}
	
	public TopicBuilder setFeature(int feature){
		topic.setFeature(feature);
		return this;
	}
	
	public TopicBuilder setLocation(String location){
		topic.setLocation(location);
		return this;
	}
	
	public TopicBuilder setActualValue(String value){
		topic.setActualValue(value);
		return this;
		
	}
	
	public TopicBuilder setPreferredValue(String value){
		topic.setPreferredValue(value);
		return this;
		
	}
	
	public TopicBuilder setTimestamp(String timestamp){
		topic.setTimestamp(timestamp);
		return this;
		
	}
	
	public Topic getTopic(){
		
		return topic;
	}
	
	

}

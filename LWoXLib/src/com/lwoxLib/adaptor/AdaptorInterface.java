package com.lwoxLib.adaptor;

import android.content.Context;
/**
 * Interface exposes method implemented by adaptor entry point class
 * @author Gianluca De Mitri
 *
 */
public interface AdaptorInterface {
	
	public void start(Context context);
	
	public void stop();

}

package com.lwox.observer;

import java.util.Observable;
import java.util.Observer;
import com.lwoxLib.topic.Topic;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
/**
 * This observer will receive the update topic notifications and it sends them in broadcast to the applications subscribed to the updated topic 
 * @author Gianluca De Mitri
 *
 */
public class HandleDataObserver implements Observer {

	Context c;
	//SensorDataBuilder builder = new SensorDataBuilder();

	public HandleDataObserver(Context context)
	{
		c = context;
	}
	@Override
	public void update(Observable observable, Object data) {

		Log.d("SERVICE", "Sending topic to subscribers...");
		Topic topic=(Topic) data;
		sendBroadcast(topic, buildTopicIntent(topic));
		Log.d("SERVICE", "Topic is sent to subscribers");
		Log.d("SERVICE", "-----------------------------");
	}

	public void sendBroadcast(Topic topic, String action)
	{
		if (topic == null)
		{
			Log.d("SERVICE", "Topic is null");
			return;
		}
		Intent intent = new Intent();
		intent.setAction(action);
		Bundle bundle = new Bundle();
		bundle.putSerializable("bundleobj", topic);
		intent.putExtra("data", bundle);
		c.sendBroadcast(intent);
	}
	
	public String buildTopicIntent(Topic topic){
		
		String intent="topic."+topic.getFeature()+"."+topic.getLocation();
		Log.d("SERVICE", "Sending on \""+intent+"\" intent");
		return intent;
		
		
	}

}

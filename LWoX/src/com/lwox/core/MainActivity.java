package com.lwox.core;

import java.io.File;
import java.util.ArrayList;
import com.lwox.parameters.Parameters;
import com.lwox.utility.StateManager;
import com.lwox.utility.Utility;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.LinearLayout;
/**
 * This is the main Activity of the LWox main-app
 * @author Gianluca De Mitri
 *
 */
public class MainActivity extends Activity implements OnCheckedChangeListener  {
	
	ArrayList<String> SelectedBox = new ArrayList<String>();
	Activity activity=this;
	boolean started=false;
	int numChecked=0;
	
	Button stop;
	Button start;
	LinearLayout linearLayout;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		linearLayout=(LinearLayout) findViewById(R.id.linearLayout1);
		

		//SharedPreferences preferences = activity.getSharedPreferences(Parameters.PREFERENCES, Context.MODE_PRIVATE);
		//preferences.edit().clear().commit();
		
		Utility.createSystemPath();
		if (!Utility.checkFile(Parameters.XML_SENSOR_FILE)) {
			Utility.initialDeviceAnalysis(this);
		}			

		Log.d("TAG", LWoxService.class.getName());
		Intent intent = new Intent(this, LWoxService.class);

		if(!Utility.isMyServiceRunning(this, LWoxService.class))
			startService(intent);

		Button stopAll= (Button) findViewById(R.id.button1);
		stopAll.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				
				SharedPreferences preferences = activity.getSharedPreferences(Parameters.PREFERENCES, Context.MODE_PRIVATE);
				preferences.edit().clear().commit();
				android.os.Process.killProcess(android.os.Process.myPid());
			}
		});


	    start= (Button) findViewById(R.id.button3);
		start.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {				
				started=false;
				ArrayList<String> adaptorToStart=new ArrayList<String>();
				for(int i=0; i<linearLayout.getChildCount(); i++){
					View view = linearLayout.getChildAt(i);
					if (view instanceof CheckBox) {
						if(((CheckBox) view).isChecked())
							adaptorToStart.add(String.valueOf(((CheckBox) view).getText()));
					}

				}

				Intent it = new Intent("com.android.startAdaptor");
				it.putStringArrayListExtra("adaptor", adaptorToStart);
				sendBroadcast(it);
				//Adaptor.loadAdaptor(activity, adaptorToStart, false);

			}
		});
		
		Button showAdaptor= (Button) findViewById(R.id.button2);
		showAdaptor.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {				

				start.setEnabled(true);
				stop.setEnabled(false);
				showAvailableAdaptorList();
				started=false;
				
			}
		});

		Button adaptorStarted= (Button) findViewById(R.id.button4);
		adaptorStarted.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				showAdaptorStartedList();
				started=true;
				start.setEnabled(false);
				SelectedBox.clear();
			}
		});

		stop= (Button) findViewById(R.id.button5);
		stop.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				/*StateManager.stopSelected(activity, SelectedBox);
				finish();*/
				StateManager.stopSelected(activity, SelectedBox);
				Intent it = new Intent("com.android.stopAdaptor");
				it.putStringArrayListExtra("adaptor", SelectedBox);
				sendBroadcast(it);
				showAdaptorStartedList();
				
			}
		});

		stop.setEnabled(false);
		start.setEnabled(false);
	}

	@Override
	public void onDestroy(){
		super.onDestroy();
		Log.d("ON DESTROY", "ON DESTROY");
	}
	

	private void showAvailableAdaptorList(){

		linearLayout.removeAllViews();
		File[] files = new File(Parameters.ADAPTOR_PATH).listFiles();
		if (files != null)
		{
			for(File file : files){

				if(!file.getName().contains("txt")){
					CheckBox checkBox = new CheckBox(this);
					checkBox.setText(file.getName());
					//checkBox.setOnCheckedChangeListener(this);
					if(StateManager.isSavedInState(activity, file.getName()))
						checkBox.setChecked(true);
					linearLayout.addView(checkBox);
				}
			}
		}
	}	
	
	
	private void showAdaptorStartedList(){

		linearLayout.removeAllViews();
		File[] files = new File(Parameters.ADAPTOR_PATH).listFiles();
		if (files != null)
		{
			for(File file : files){

				if(!file.getName().contains("txt")){
					CheckBox checkBox = new CheckBox(this);
					checkBox.setText(file.getName());
					checkBox.setOnCheckedChangeListener(this);
					if(StateManager.isSavedInState(activity, file.getName()))
						//checkBox.setChecked(true);
						linearLayout.addView(checkBox);
				}
			}
		}
	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		
		if(started){
			Log.d("NAME", String.valueOf(buttonView.getText()));
			if(isChecked){
				SelectedBox.add((String) buttonView.getText());
				numChecked++;
			}
			else
			{
				SelectedBox.remove(buttonView.getText());
				numChecked--;
			}
		}
		if(numChecked>0)
			stop.setEnabled(true);
		else
			stop.setEnabled(false);
	}
}

package com.lwox.dao;

/**
 * This is a generic Dao that manages the connection and the communications with the database. 
 */
import android.content.Context;
import android.os.Environment;
import android.util.Log;

import com.db4o.Db4oEmbedded;
import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;
import com.db4o.config.EmbeddedConfiguration;
import com.db4o.ext.DatabaseClosedException;
import com.db4o.ext.DatabaseReadOnlyException;
import com.db4o.ext.Db4oIOException;
import com.db4o.query.Predicate;
import com.db4o.query.Query;
import com.db4o.query.QueryComparator;
import com.lwox.parameters.Parameters;

import java.io.IOException;
import java.util.Comparator;
import java.util.Observable;

public abstract class Db4OGenericDao<T> extends Observable{

    private static ObjectContainer oc = null;
    private Context context;

    private String DB_NAME="topic_map.db4o";

    /**
     * @param ctx
     */
    public Db4OGenericDao(Context ctx) {
        setContext(ctx);
    }

    /**
     * Create, open and close the database
     */
    protected ObjectContainer db() {

        if (oc==null || oc.ext().isClosed()){
            synchronized (this){
                try {
                    if (oc == null || oc.ext().isClosed()) {
                        oc = Db4oEmbedded.openFile(dbConfig(), db4oDBFullPath());
                    }

                    return oc;

                } catch (Exception ie) {
                    Log.e(Db4OGenericDao.class.getName(), ie.toString());
                    throw new IllegalStateException("error while init the db4o database", ie);
                }
            }
        }else
            return oc;
    }

    /**
     * Configure the behavior of the database
     */

    private EmbeddedConfiguration dbConfig() throws IOException {
        EmbeddedConfiguration configuration = Db4oEmbedded.newConfiguration();

        return configuration;
    }

    /**
     * Returns the path for the database location
     */

    private String db4oDBFullPath() {
    
    	return Parameters.DB_PATH+DB_NAME;
        
    }





    public  void store(T o) throws DatabaseClosedException, DatabaseReadOnlyException {
        ObjectContainer oc=db();
        oc.store(o);
        oc.commit();

    }

    public void close() {
            if (oc != null)
                oc.close();

    }

    public  <TargetType> ObjectSet<TargetType> query(Predicate<TargetType> targetTypePredicate) throws Db4oIOException, DatabaseClosedException {
        ObjectContainer oc=db();
        return oc.query(targetTypePredicate);
    }

    public  <T> ObjectSet<T> queryByExample(T o) throws Db4oIOException, DatabaseClosedException {
        ObjectContainer oc=db();
        return oc.queryByExample(o);
    }



    public <TargetType> ObjectSet<TargetType> query(Class<TargetType> targetTypeClass) throws Db4oIOException, DatabaseClosedException {
        ObjectContainer oc=db();
        return oc.query(targetTypeClass);
    }

    public Query query() throws DatabaseClosedException {
        ObjectContainer oc=db();
        return oc.query();
    }

    public  void delete(T o) throws Db4oIOException, DatabaseClosedException, DatabaseReadOnlyException {
        ObjectContainer oc=db();
        oc.delete(o);
        oc.commit();
    }

    public  <TargetType> ObjectSet<TargetType> query(Predicate<TargetType> targetTypePredicate, Comparator<TargetType> targetTypeComparator) throws Db4oIOException, DatabaseClosedException {
        ObjectContainer oc=db();
        return oc.query(targetTypePredicate, targetTypeComparator);
    }



    public  <TargetType> ObjectSet<TargetType> query(Predicate<TargetType> targetTypePredicate, QueryComparator<TargetType> targetTypeQueryComparator) throws Db4oIOException, DatabaseClosedException {
        ObjectContainer oc=db();
        return oc.query(targetTypePredicate, targetTypeQueryComparator);
    }

	public Context getContext() {
		return context;
	}

	public void setContext(Context context) {
		this.context = context;
	}
}
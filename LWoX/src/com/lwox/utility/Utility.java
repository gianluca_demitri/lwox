package com.lwox.utility;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.jar.JarFile;
import com.lwox.parameters.Parameters;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.content.Context;
import android.content.pm.FeatureInfo;
import android.content.pm.PackageManager;
import android.support.v4.content.ContextCompat;
import android.util.Log;

public class Utility implements Parameters {

	/**
	 * This method filters the android sensor list 
	 * @param list
	 * @return
	 */
	public List<String> cleanFeatures(FeatureInfo[] list){
		List<String> fResult=new ArrayList<String>();

		for(int i=0; i<list.length-1; i++){
			if(list[i].name.contains("hardware")){
				String feature=list[i].name.replace("android.hardware.",""); 
				fResult.add(feature);
			}			
		} 
		return fResult;

	}
	
	/**
	 * This method checks if service is running
	 * @param context
	 * @param serviceClass
	 * @return
	 */
	public static boolean isMyServiceRunning(Context context, Class<?> serviceClass) {
		ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
		for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
			if (serviceClass.getName().equals(service.service.getClassName())) {
				return true;
			}
		}
		return false;
	}

	/**
	 * This method checks if a file with a specified path exists.
	 * @param path
	 * @return
	 */
	public static boolean checkFile(String path){
		File newxmlfile = new File(path);
		if(newxmlfile.exists())
			return true;
		else 
			return false;
	}
	
	/**
	 * This methods carries out an initial device analysis producing a Smartphone sensor list 
	 * @param context
	 */
	public static void initialDeviceAnalysis(Context context)
	{
		Log.d("SERVICE", "Initial device analysing");
		PackageManager pm = (PackageManager)context.getPackageManager();
		FeatureInfo[] featuresList = pm.getSystemAvailableFeatures();
		List<String> slist=new Utility().cleanFeatures(featuresList);
		XmlParser parser=new XmlParser(slist);
		parser.createXml();
		Log.d("SERVICE", "Initial device analysis completed");
	}
	
	/**
	 * This methods create the service working directories path.
	 */
	public static void createSystemPath()
	{
		if (!checkFile(XML_PATH)) {
			new File(XML_PATH).mkdirs();
		}
		if (!checkFile(DB_PATH))
		{
			Log.d("ERROR", "directory doesn't exist");
			new File(DB_PATH).mkdirs();
		}
		if (!checkFile(ADAPTOR_PATH))
		{
			Log.d("ERROR", "directory doesn't exist");
			new File(ADAPTOR_PATH).mkdirs();
		}
	}

	// decompressXML -- Parse the 'compressed' binary form of Android XML docs 
	// such as for AndroidManifest.xml in .apk files
	
	/**
	 * This method gets the adaptor permissions.
	 * @param path
	 * @return
	 */
	public static ArrayList<String> getAdaptorPermission(String path) {
		ArrayList<String> result=null;
		try {
			JarFile jf = new JarFile(path);
			InputStream is = jf.getInputStream(jf.getEntry("AndroidManifest.xml"));
			byte[] xml = new byte[is.available()];
			int br = is.read(xml);

			result=ManifestDecoder.decompressXML(xml);
			jf.close();
		} catch (Exception ex) {		      
			ex.printStackTrace();
		}
		return result;
	} // end of getIntents

	/**
	 * This method checks if the adaptor permissions is enabled in LWoX service
	 * @param perm
	 * @param activity
	 * @return
	 */
	public static boolean checkPermission(ArrayList<String> perm, Context activity){
		// Here, thisActivity is the current activity
		Iterator iterator=perm.iterator();
		String current;
		while(iterator.hasNext()){

			current=(String) iterator.next();
			if (ContextCompat.checkSelfPermission(activity,current)!= PackageManager.PERMISSION_GRANTED) {
				return false;
			}
		}
		return true;
	}
}
